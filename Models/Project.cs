﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace lagalt.Models
{
    public class Project
    {
        public int Id { set; get; }

        public List<ProjectSkill> Skills { set; get; }
        public Project()
        {
            Skills = new List<ProjectSkill>();
        }
    }
}
