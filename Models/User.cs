﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace lagalt.Models
{
    public class User
    {

        public int Id { set; get; }

        public List<UserSkill> Skills { set; get; }

        public User()
        {
            Skills = new List<UserSkill>();
        }




    }
}
