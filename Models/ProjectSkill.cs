﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace lagalt.Models
{
    public class ProjectSkill
    {
        public int Id { set; get; }
        [Key, Column(Order = 1)]
        public int ProjectId { set; get; }
        [Key, Column(Order = 2)]
        public int SkillId { set; get; }
        public Project Project { set; get; }
        public Skill Skill { set; get; }

        public ProjectSkill()
        {

        }
        public ProjectSkill(Project project, Skill skill)
        {
            Project = project;
            Skill = skill;
        }


    }
}
