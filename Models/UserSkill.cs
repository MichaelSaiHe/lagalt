﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace lagalt.Models
{
    public class UserSkill
    {
        public int Id { set; get; }
        [Key, Column(Order = 1)]
        public int UserId { set; get; }
        [Key, Column(Order = 2)]
        public int SkillId { set; get; }
        public User User { set; get; }
        public Skill Skill { set; get; }
    }
}
