﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace lagalt.Models
{
    public class UserProject
    {
        public UserProject()
        {

        }
        public UserProject(User user, Project project)
        {
            User = user;
            Project = project;
        }
        [Key, Column(Order = 1)]
        public int UserId { get; set; }
        [Key, Column(Order = 2)]
        public int ProjectId { get; set; }
        public User User { get; set; }
        public Project Project { get; set; }

    }
}
