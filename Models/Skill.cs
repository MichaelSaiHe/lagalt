﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Threading.Tasks;

namespace lagalt.Models
{
    public class Skill
    {
        public int Id { get; set; }
        public String Description { get; set;  }
        public Skill()
        {

        }
        public Skill (String description)
        {
            Description = description;
        }
    }
}
